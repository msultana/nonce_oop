# Nonce_oop

Package that implements the WordPress Nonces functionality (wp_nonce_*()) in an object orientated way.

## Requirements

- PHP 5.6+
- Composer
- WordPress 4.8.3+

## Installation

Install with [Composer](https://getcomposer.org):

```sh
$ composer require msultana/nonce_oop:dev-master
```

### Run the tests

To run tests, executes commands below:

```sh
$ cd nonces_oop
$ composer install
$ .vendor/bin/phpunit
```

Features
--------
* Unit-Testing with PHPUnit
* Easy to use to any framework or even a plain php file


## Usage

Initialize the nonce object

**```$create = new WpnonceCreate( 'action_name', 'nonce_parameter_name' );```**

Then, to generate the nonce use the create_nonce() method:

**```$nonce = $create->create_nonce();```**
  
Generate a url with nonce parameter:

**```$url = $create->create_nonce_url( 'http://github.com' );```**

Generate nonce field:

**``$field = $create->create_nonce_field();``**  

Nonce validation:

**``$is_valid = $create->validate_nonce($nonce);``**

To validate a nonce received in a page through request (GET or POST) use the validate_request() method:

**```$is_valid = $create->validate_request();```**

## Credits
Muryam Sultana
muryam.sultana@gmail.com
